const mongoose = require('mongoose');
const schemaPerson = require('./schemas/personas');
const Schema = mongoose.Schema;

const Personas = mongoose.model('personas', new Schema(schemaPerson));

// Mongo DB
const initMongo = () => {
    const optionsDB = {
        socketTimeoutMS: 30000,
        keepAlive: true,
        reconnectTries: 30000,
        useNewUrlParser: true
    };
    const mongoDB = 'mongodb://127.0.0.1/personas';
    
    mongoose.connect(mongoDB, optionsDB);
    mongoose.Promise = global.Promise;
    const db = mongoose.connection;
    db.on('error', console.error.bind(console, 'MongoDB connection error:'));
    
};

const getModel = (id = null) => {
    if (id) return Personas.find({id: Number(id)}).exec();
    return Personas.find().exec();
};

module.exports = {
    initMongo,
    getModel    
}