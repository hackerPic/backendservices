const express = require('express');
const morgan = require('morgan');
const { initMongo, getModel } = require('./mongo');
const app = express();

//load getters DB


//Set
app.set('port', 4500);

//Middleware
app.use(express.json());
app.use(morgan('common'));

//Init MongoDB
initMongo();


app.get('/users/:idUser', async (req, res) => {
    console.log('fd', req.params)
    res.json({
        code: 200,
        status: 'OK',
        payload: await getModel(req.params.idUser)
    });
});

app.post('/user', (req, res) => {
    res.send({
        status: 'OK',
        code: 200,
        payload: {
            nombre: 'Hannah',
            paterno: 'Narvaez',
            edad: 37
        }
    });
});

app.use(express.static('public'))

app.listen(app.get('port'), () => {
    console.log(`Services is listening in port ${app.get('port')}`);
});